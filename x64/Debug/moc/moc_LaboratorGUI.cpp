/****************************************************************************
** Meta object code from reading C++ file 'LaboratorGUI.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.0.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../LaboratorGUI.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'LaboratorGUI.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.0.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LaboratorGUI_t {
    const uint offsetsAndSize[56];
    char stringdata0[383];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(offsetof(qt_meta_stringdata_LaboratorGUI_t, stringdata0) + ofs), len 
static const qt_meta_stringdata_LaboratorGUI_t qt_meta_stringdata_LaboratorGUI = {
    {
QT_MOC_LITERAL(0, 12), // "LaboratorGUI"
QT_MOC_LITERAL(13, 11), // "show_window"
QT_MOC_LITERAL(25, 0), // ""
QT_MOC_LITERAL(26, 13), // "QApplication&"
QT_MOC_LITERAL(40, 9), // "aplicatie"
QT_MOC_LITERAL(50, 14), // "sortari_window"
QT_MOC_LITERAL(65, 13), // "sortare_titlu"
QT_MOC_LITERAL(79, 17), // "sortare_descriere"
QT_MOC_LITERAL(97, 18), // "sortare_tip_durata"
QT_MOC_LITERAL(116, 15), // "filtrare_durata"
QT_MOC_LITERAL(132, 12), // "filtrare_tip"
QT_MOC_LITERAL(145, 19), // "adaugare_activitate"
QT_MOC_LITERAL(165, 14), // "adaugare_lista"
QT_MOC_LITERAL(180, 13), // "export_fisier"
QT_MOC_LITERAL(194, 14), // "stergere_lista"
QT_MOC_LITERAL(209, 14), // "generare_lista"
QT_MOC_LITERAL(224, 14), // "populate_tabel"
QT_MOC_LITERAL(239, 22), // "actualizare_activitate"
QT_MOC_LITERAL(262, 19), // "stergere_activitate"
QT_MOC_LITERAL(282, 12), // "double_click"
QT_MOC_LITERAL(295, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(312, 8), // "unselect"
QT_MOC_LITERAL(321, 14), // "filtrare_color"
QT_MOC_LITERAL(336, 14), // "schimba_window"
QT_MOC_LITERAL(351, 12), // "clear_window"
QT_MOC_LITERAL(364, 4), // "undo"
QT_MOC_LITERAL(369, 4), // "redo"
QT_MOC_LITERAL(374, 8) // "populate"

    },
    "LaboratorGUI\0show_window\0\0QApplication&\0"
    "aplicatie\0sortari_window\0sortare_titlu\0"
    "sortare_descriere\0sortare_tip_durata\0"
    "filtrare_durata\0filtrare_tip\0"
    "adaugare_activitate\0adaugare_lista\0"
    "export_fisier\0stergere_lista\0"
    "generare_lista\0populate_tabel\0"
    "actualizare_activitate\0stergere_activitate\0"
    "double_click\0QListWidgetItem*\0unselect\0"
    "filtrare_color\0schimba_window\0"
    "clear_window\0undo\0redo\0populate"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LaboratorGUI[] = {

 // content:
       9,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    1,  152,    2, 0x0a,    0 /* Public */,
       5,    0,  155,    2, 0x0a,    2 /* Public */,
       6,    0,  156,    2, 0x0a,    3 /* Public */,
       7,    0,  157,    2, 0x0a,    4 /* Public */,
       8,    0,  158,    2, 0x0a,    5 /* Public */,
       9,    0,  159,    2, 0x0a,    6 /* Public */,
      10,    0,  160,    2, 0x0a,    7 /* Public */,
      11,    0,  161,    2, 0x0a,    8 /* Public */,
      12,    0,  162,    2, 0x0a,    9 /* Public */,
      13,    0,  163,    2, 0x0a,   10 /* Public */,
      14,    0,  164,    2, 0x0a,   11 /* Public */,
      15,    0,  165,    2, 0x0a,   12 /* Public */,
      16,    0,  166,    2, 0x0a,   13 /* Public */,
      17,    0,  167,    2, 0x0a,   14 /* Public */,
      18,    0,  168,    2, 0x0a,   15 /* Public */,
      19,    1,  169,    2, 0x0a,   16 /* Public */,
      21,    1,  172,    2, 0x0a,   18 /* Public */,
      22,    0,  175,    2, 0x0a,   20 /* Public */,
      23,    0,  176,    2, 0x0a,   21 /* Public */,
      24,    0,  177,    2, 0x0a,   22 /* Public */,
      25,    0,  178,    2, 0x0a,   23 /* Public */,
      26,    0,  179,    2, 0x0a,   24 /* Public */,
      27,    0,  180,    2, 0x0a,   25 /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 20,    2,
    QMetaType::Void, 0x80000000 | 20,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void LaboratorGUI::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<LaboratorGUI *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->show_window((*reinterpret_cast< QApplication(*)>(_a[1]))); break;
        case 1: _t->sortari_window(); break;
        case 2: _t->sortare_titlu(); break;
        case 3: _t->sortare_descriere(); break;
        case 4: _t->sortare_tip_durata(); break;
        case 5: _t->filtrare_durata(); break;
        case 6: _t->filtrare_tip(); break;
        case 7: _t->adaugare_activitate(); break;
        case 8: _t->adaugare_lista(); break;
        case 9: _t->export_fisier(); break;
        case 10: _t->stergere_lista(); break;
        case 11: _t->generare_lista(); break;
        case 12: _t->populate_tabel(); break;
        case 13: _t->actualizare_activitate(); break;
        case 14: _t->stergere_activitate(); break;
        case 15: _t->double_click((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 16: _t->unselect((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 17: _t->filtrare_color(); break;
        case 18: _t->schimba_window(); break;
        case 19: _t->clear_window(); break;
        case 20: _t->undo(); break;
        case 21: _t->redo(); break;
        case 22: _t->populate(); break;
        default: ;
        }
    }
}

const QMetaObject LaboratorGUI::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_LaboratorGUI.offsetsAndSize,
    qt_meta_data_LaboratorGUI,
    qt_static_metacall,
    nullptr,
qt_incomplete_metaTypeArray<qt_meta_stringdata_LaboratorGUI_t

, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<QApplication &, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<QListWidgetItem *, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<QListWidgetItem *, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>


>,
    nullptr
} };


const QMetaObject *LaboratorGUI::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LaboratorGUI::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LaboratorGUI.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int LaboratorGUI::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 23;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
