/********************************************************************************
** Form generated from reading UI file 'LaboratorGUI.ui'
**
** Created by: Qt User Interface Compiler version 6.0.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LABORATORGUI_H
#define UI_LABORATORGUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LaboratorGUIClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *LaboratorGUIClass)
    {
        if (LaboratorGUIClass->objectName().isEmpty())
            LaboratorGUIClass->setObjectName(QString::fromUtf8("LaboratorGUIClass"));
        LaboratorGUIClass->resize(600, 400);
        menuBar = new QMenuBar(LaboratorGUIClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        LaboratorGUIClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(LaboratorGUIClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        LaboratorGUIClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(LaboratorGUIClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        LaboratorGUIClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(LaboratorGUIClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        LaboratorGUIClass->setStatusBar(statusBar);

        retranslateUi(LaboratorGUIClass);

        QMetaObject::connectSlotsByName(LaboratorGUIClass);
    } // setupUi

    void retranslateUi(QMainWindow *LaboratorGUIClass)
    {
        LaboratorGUIClass->setWindowTitle(QCoreApplication::translate("LaboratorGUIClass", "LaboratorGUI", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LaboratorGUIClass: public Ui_LaboratorGUIClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LABORATORGUI_H
