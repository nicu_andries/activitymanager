#pragma once
#include <cctype>
#include <iostream>
#include <vector>
#include "Repository.h"


class Validator {
public:
	void validare_activitate(const Activitate& activitate);

	void validare_adaugare(const Activitate& activitate, const std::vector <Activitate> lista);

	void validare_modificare_stergere(std::string titlu, std::vector <Activitate> lista);

	void TestAfisare();
};


class ValidateException {
	std::vector<std::string> messages;

public:
	ValidateException(const std::vector<std::string>& erori) :
		messages{ erori } {};

	std::vector<std::string> get_exceptie() {
		return messages;
	}

	friend std::ostream& operator<<(std::ostream& out, const ValidateException& err);
};

std::ostream& operator<<(std::ostream& out, const ValidateException& err);

