#pragma once
#include <iostream>
#include <assert.h>
#include <string>

class Activitate {
	std::string titlu;
	std::string descriere;
	std::string tip;
	int durata;

public:
	Activitate(const std::string title, const std::string description, const std::string type, int time) :
		titlu{ title },
		descriere{ description },
		tip{ type },
		durata{ time }
	{};

	std::string get_titlu() const;

	std::string get_descriere() const;

	std::string get_tip() const;

	int get_durata() const noexcept;

	std::string get_str() const;
};

