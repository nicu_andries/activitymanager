#include "Validator.h"

void Validator::validare_activitate(const Activitate& activitate) {
	std::vector <std::string> messages;
	if (activitate.get_titlu().size() == 0) messages.push_back("Titlul nu poate fi vid!");
	if (activitate.get_durata() < 0) messages.push_back("Durata nu poate fi negativa!");
	if (activitate.get_descriere().size() == 0) messages.push_back("Descrierea nu poate fi vida!");
	if (activitate.get_tip().size() == 0) messages.push_back("Tipul nu poate fi vid!");
	if (messages.size() > 0)
		throw ValidateException(messages);
}

void Validator::validare_adaugare(const Activitate& activitate, const std::vector <Activitate> lista) {
	std::vector <std::string> messages;
	for (auto& obiect : lista)
		if (obiect.get_titlu() == activitate.get_titlu() &&
			obiect.get_descriere() == activitate.get_descriere())
			messages.push_back("Aceasta activitate deja exista!");
	if (messages.size() > 0) throw ValidateException(messages); }

void Validator::validare_modificare_stergere(std::string titlu, std::vector <Activitate> lista) {
	for (auto& obiect : lista)
		if (obiect.get_titlu() == titlu) return;
	std::vector <std::string> messages;
	messages.push_back("Aceasta activitate nu exista!");
	throw ValidateException(messages); }

void Validator::TestAfisare() {
	std::vector <std::string> messages;
	messages.push_back("");
	throw ValidateException(messages); }

std::ostream& operator<<(std::ostream& out, const ValidateException& erori) {
	for (const auto& message : erori.messages)
		out << message << "\n";
	return out;
}

