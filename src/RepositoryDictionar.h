#pragma once

#include "Domain.h"
#include <algorithm>
#include "Repository.h"
#include <map>

class RepositoryDictionar : public Repository {
private:
	std::map <std::string, Activitate> dictionar;
	vector <Activitate> lista;
	double probabilitate;
	void exceptie();

public:
	RepositoryDictionar() = default;

	RepositoryDictionar(double probabilitate) : Repository(), probabilitate{ probabilitate } {}

	RepositoryDictionar(const RepositoryDictionar& repository) = delete;

	void adaugare_activitate(const Activitate& activitate) override;

	std::vector <Activitate>& get_all() noexcept override;

	int actualizare_activitate(const std::string titlu, const Activitate& activitate) override;

	int stergere_activitate(const std::string titlu) override;
};
