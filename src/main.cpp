#include "LaboratorGUI.h"
#include <QtWidgets/QApplication>
#include "Repository.h"
//#include "Tests.h"
#include "Service.h"

using namespace std;

int main(int argc, char *argv[])
{
	//Teste teste;
	//teste.run_all_tests();
	QApplication aplicatie(argc, argv);
	Validator validator;
	RepositoryMemory repository;
	Service service{ repository, validator };
	LaboratorGUI interfata{ service };
	interfata.sortari_window();
	interfata.show_window(aplicatie);

	return aplicatie.exec();
}

