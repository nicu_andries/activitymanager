#pragma once
#include <string>

using std::string;

class Exception
{
private:
	string message;

public:
	Exception(string message) : message{ message } {

	}

	string get_mesaje() const
	{
		return message;
	}
};


class RepositoryException : public Exception
{
public:
	RepositoryException(string message) : Exception(message) {}
};


class ProbabilitateException : public Exception
{
public:
	ProbabilitateException(string message) : Exception{ message } {}
};
