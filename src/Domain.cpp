#include "Domain.h"
#include <string>

std::string Activitate::get_titlu() const {
	return titlu;
}

std::string Activitate::get_descriere() const {
	return descriere;
}

std::string Activitate::get_tip() const {
	return tip;
}

int Activitate::get_durata() const noexcept {
	return durata;
}

std::string Activitate::get_str() const {
	std::string durata_str;
	durata_str = std::to_string(durata);
	return titlu + " " + descriere + " " + tip + " " + durata_str;
}