#include "Tests.h"
#include "Domain.h"
#include "Repository.h"
#include "RepositoryDictionar.h"
#include "Service.h"
#include "Validator.h"
#include "Exceptii.h"
#include <string.h>


void Teste::test_domain() const {
	Activitate activitate = Activitate("Activitate", "Box", "Tip", 1);
	assert(activitate.get_titlu() == "Activitate");
	assert(activitate.get_durata() == 1);
	assert(activitate.get_descriere() == "Box");
	assert(activitate.get_tip() == "Tip");
	assert(activitate.get_str() == "Activitate Box Tip 1");
}


void Teste::test_repository_dictionar() const {
	RepositoryDictionar dictionar{ 1.0 };
	dictionar.adaugare_activitate(Activitate("Activitate", "Box", "Tip", 1));
	dictionar.adaugare_activitate(Activitate("Pin-pong", "Joc", "Yoga", 2));
	assert(dictionar.get_all().size() == 2);

	Activitate activitate{ dictionar.get_all().at(0) };
	assert(activitate.get_titlu() == "Activitate");
	assert(activitate.get_durata() == 1);
	assert(activitate.get_descriere() == "Box");
	assert(activitate.get_tip() == "Tip");

	dictionar.stergere_activitate("Activitate");
	assert(dictionar.get_all().size() == 1);
	activitate = dictionar.get_all().at(0);
	assert(activitate.get_titlu() == "Pin-pong");
	assert(activitate.get_durata() == 2);
	assert(activitate.get_descriere() == "Joc");
	assert(activitate.get_tip() == "Yoga");

	dictionar.actualizare_activitate("Pin-pong", Activitate("Alergare", "Afara", "Sarituri", 2));
	activitate = dictionar.get_all().at(0);
	assert(activitate.get_titlu() == "Alergare");
	assert(activitate.get_durata() == 2);
	assert(activitate.get_descriere() == "Afara");
	assert(activitate.get_tip() == "Sarituri");
	assert(dictionar.stergere_activitate("Activitate") == 1);

	RepositoryDictionar dictionar2{ 0.0 };
	try {
		dictionar2.adaugare_activitate(Activitate("Activitate", "Box", "Tip", 1));
	}
	catch (const ProbabilitateException& exceptie) {
		assert(exceptie.get_mesaje() == "Probabilitatea de a arunca o eroare este prea mare!\n");
	}
}


void Teste::test_repository() const {
	RepositoryMemory lista;
	lista.adaugare_activitate(Activitate("Activitate", "Box", "Tip", 1));
	lista.adaugare_activitate(Activitate("Pin-pong", "Joc", "Yoga", 2));
	assert(lista.get_all().size() == 2);

	try {
		lista.adaugare_activitate(Activitate("Activitate", "Box", "Tip", 1));
	}
	catch (const RepositoryException& exceptie) {
		assert(exceptie.get_mesaje() == "Activitatea data deja exista!\n");
	}

	Activitate activitate{ lista.get_all().at(0) };
	assert(activitate.get_titlu() == "Activitate");
	assert(activitate.get_durata() == 1);
	assert(activitate.get_descriere() == "Box");
	assert(activitate.get_tip() == "Tip");

	lista.stergere_activitate("Activitate");
	assert(lista.get_all().size() == 1);
	activitate = lista.get_all().at(0);
	assert(activitate.get_titlu() == "Pin-pong");
	assert(activitate.get_durata() == 2);
	assert(activitate.get_descriere() == "Joc");
	assert(activitate.get_tip() == "Yoga");

	lista.actualizare_activitate("Pin-pong", Activitate("Alergare", "Afara", "Sarituri", 2));
	activitate = lista.get_all().at(0);
	assert(activitate.get_titlu() == "Alergare");
	assert(activitate.get_durata() == 2);
	assert(activitate.get_descriere() == "Afara");
	assert(activitate.get_tip() == "Sarituri");

	assert(lista.stergere_activitate("Activitate") == -1);
}


void Teste::test_service() const {
	RepositoryMemory lista;
	Validator validator;
	Service serv{ lista, validator };
	serv.adaugare_activitate(Activitate("Activitate", "Box", "Xenon", 2));

	serv.generare_random(1);
	assert(serv.get_all_lista_activitati().size() == 1);
	serv.ExportCSV();
	serv.stergere_lista_activitati();
	assert(serv.get_all_lista_activitati().size() == 0);

	serv.adaugare_activitate(Activitate("Natatie", "Sport", "Umed", 3));
	serv.adaugare_activitate(Activitate("Distractie", "Creativa", "Tenis", 4));
	assert(serv.get_all().size() == 3);

	serv.adaugare_activitate_lista("Activitate");
	assert(serv.get_all_lista_activitati().size() == 1);
	serv.stergere_lista_activitati();

	Activitate activitate{ serv.get_all().at(0) };
	assert(activitate.get_titlu() == "Activitate");
	assert(activitate.get_durata() == 2);
	assert(activitate.get_descriere() == "Box");
	assert(activitate.get_tip() == "Xenon");

	int i = serv.get_by_title("Activitate");
	assert(i == 0);
	i = serv.get_by_title("Activity");
	assert(i == -1);

	auto result = serv.filtrare_dupa_durata(3);
	assert(result.size() == 2);

	activitate = result.at(1);
	assert(activitate.get_titlu() == "Natatie");
	assert(activitate.get_durata() == 3);
	assert(activitate.get_descriere() == "Sport");
	assert(activitate.get_tip() == "Umed");

	result = serv.filtrare_dupa_tip("Xenon");
	assert(result.size() == 1);
	activitate = result.at(0);
	assert(activitate.get_titlu() == "Activitate");
	assert(activitate.get_durata() == 2);
	assert(activitate.get_descriere() == "Box");
	assert(activitate.get_tip() == "Xenon");

	RepositoryMemory lista2;
	Service serv2{ lista2, validator };
	serv2.adaugare_activitate(Activitate("Activitate", "Box", "Xenon", 5));
	serv2.adaugare_activitate(Activitate("Natatie", "Sport", "Umed", 10));
	serv2.adaugare_activitate(Activitate("Distractie", "Creativa", "Xenon", 4));

	auto result2 = serv2.sortare_dupa_titlu();
	activitate = result2.at(1);
	assert(activitate.get_titlu() == "Distractie");
	assert(activitate.get_durata() == 4);
	assert(activitate.get_descriere() == "Creativa");
	assert(activitate.get_tip() == "Xenon");

	result2 = serv2.sortare_dupa_descriere();
	activitate = result2.at(1);
	assert(activitate.get_titlu() == "Distractie");
	assert(activitate.get_durata() == 4);
	assert(activitate.get_descriere() == "Creativa");
	assert(activitate.get_tip() == "Xenon");

	result2 = serv2.sortare_dupa_tip_durata();
	activitate = result2.at(1);
	assert(activitate.get_titlu() == "Distractie");
	assert(activitate.get_durata() == 4);
	assert(activitate.get_descriere() == "Creativa");
	assert(activitate.get_tip() == "Xenon");

	RepositoryMemory lista3;
	Service serv3{ lista3, validator };
	serv3.adaugare_activitate(Activitate("Activitate", "Box", "Xenon", 5));
	serv3.adaugare_activitate(Activitate("Natatie", "Sport", "Umed", 10));
	serv3.adaugare_activitate(Activitate("Distractie", "Creativa", "Xenon", 4));

	assert(serv.actualizare_activitate("Activity", Activitate("Activitate", "Box", "Xenon", 5)) == -1);
	serv.actualizare_activitate("Activitate", Activitate("Creatie", "Activitate", "Afara", 10));
	activitate = serv.get_all().at(0);
	assert(activitate.get_titlu() == "Creatie");
	assert(activitate.get_durata() == 10);
	assert(activitate.get_descriere() == "Activitate");
	assert(activitate.get_tip() == "Afara");

	assert(serv.stergere_activitate("Activity") == -1);
	serv.stergere_activitate("Distractie");
	assert(serv.get_all().size() == 2);

	RepositoryMemory lista7;
	Service serv7{ lista7, validator };
	try { serv7.Undo(); }
	catch (ValidateException&) {};
	serv7.adaugare_activitate(Activitate("Activitate", "Box", "Xenon", 5));
	serv7.Undo();
	assert(serv7.get_all().size() == 0);
	serv7.adaugare_activitate(Activitate("Activitate", "Box", "Xenon", 5));
	serv7.stergere_activitate("Activitate");
	serv7.Undo();
	assert(serv7.get_all().size() == 1);
	serv7.actualizare_activitate("Activitate", Activitate("Natatie", "Sport", "Umed", 10));
	serv7.Undo();
	assert(serv7.get_all().size() == 1);

	RepositoryMemory lista14;
	Service serv14{ lista14, validator };
	serv14.adaugare_activitate(Activitate("Activitate", "Box", "Xenon", 5));
	serv14.adaugare_activitate(Activitate("Natatie", "Sport", "Umed", 10));
	assert(serv14.get_all().size() == 2);
	serv14.Undo(); serv14.Undo();
	assert(serv14.get_all().size() == 0);
	serv14.Redo(); serv14.Redo();
	assert(serv14.get_all().size() == 2);
	serv14.Undo();
	assert(serv14.get_all().size() == 1);

	serv14.Redo();
	assert(serv14.get_all().size() == 2);
	serv14.Redo();
	assert(serv14.get_all().size() == 2);
	serv14.Undo();
	assert(serv14.get_all().size() == 1);
	serv14.adaugare_activitate(Activitate("Distractie", "Creativa", "Xenon", 4));
	serv14.Redo();
	assert(serv14.get_all().size() == 2);
	serv14.Undo(); serv14.Undo();
	assert(serv14.get_all().size() == 0);
	serv14.Redo(); serv14.Redo();
	assert(serv14.get_all().size() == 2);

	serv14.actualizare_activitate("Distractie", Activitate("Distractie2", "Creativa", "Xenon", 4));
	serv14.Undo();
	serv14.Redo();
	assert(serv14.get_all().size() == 2);
	serv14.Undo();

	serv14.stergere_activitate("Activitate");
	assert(serv14.get_all().size() == 1);
	serv14.Undo();
	serv14.Redo();
	assert(serv14.get_all().size() == 1);
	serv14.Undo();
	serv14.raport();
}


void Teste::test_validator() const {
	Validator validator;
	Activitate activitate = Activitate("Activitate", "Box", "Tip", -1);
	try { validator.validare_activitate(activitate); }
	catch (ValidateException&) {
		assert(true);
		std::cout << "";
	}
	RepositoryMemory repository;
	activitate = Activitate("Activitate", "Box", "Tip", 1); repository.adaugare_activitate(activitate);
	try { validator.validare_adaugare(activitate, repository.get_all()); }
	catch (ValidateException&) {
		assert(true);
	}
	try { validator.validare_modificare_stergere("Altceva", repository.get_all()); }
	catch (ValidateException&) {
		assert(true);
	}
	try { validator.TestAfisare(); }
	catch (const ValidateException& message) {
		std::cout << message;
	}
}


void Teste::run_all_tests() const {
	test_domain();
	test_repository();
	test_repository_dictionar();
	test_service();
	test_validator();
}