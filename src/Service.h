#pragma once

#include "Repository.h"
#include "Validator.h"
#include <chrono>
#include <functional>
#include <fstream>
#include <memory>
#include <random>
#include <string>
#include <unordered_map>
#include "CosCRUDGUI.h"

using std::unique_ptr;
using namespace std;

typedef bool(*functie) (const Activitate&, const Activitate&);

class ListaActivitati {
	std::vector <Activitate> lista_activitati;

public:
	ListaActivitati() = default;

	void adaugare_activitate(const Activitate& activitate) {
		lista_activitati.push_back(activitate);
	}

	const std::vector<Activitate>& get_all_lista_activitati() const noexcept {
		return lista_activitati;
	}

	void stergere_lista_activitati() noexcept {
		lista_activitati.clear();
	}
};

class Undo {
public:
	virtual void DoUndo() = 0;
	virtual void DoRedo() = 0;
	virtual ~Undo() = default;
};

class UndoAdd : public Undo {
	Activitate activitate;
	Repository& repository;
public:
	UndoAdd(Repository& repo, const Activitate& activity) : repository{ repo }, activitate{ activity } {}
	void DoUndo() override {
		repository.stergere_activitate(activitate.get_titlu());
	}
	void DoRedo() override {
		repository.adaugare_activitate(activitate);
	}
};

class UndoDelete : public Undo {
	Activitate activitate;
	Repository& repository;
public:
	UndoDelete(Repository& repo, const Activitate& activity) noexcept : repository{ repo }, activitate{ activity } {}
	void DoUndo() override {
		repository.adaugare_activitate(activitate);
	}
	void DoRedo() override { repository.stergere_activitate(activitate.get_titlu()); }
};

class UndoUpdate : public Undo {
	Activitate activitate;
	std::string titlu;
	Repository& repository;
public:
	UndoUpdate(Repository& repo, std::string titl, const Activitate& activity) : repository{ repo }, titlu{ titl }, activitate{ activity } {};
	void DoUndo() override {
		repository.actualizare_activitate(titlu, activitate);
	}
	void DoRedo() noexcept override { ; }
};

class Service {
	Repository& repository;
	Validator& validator;
	CosCRUDGUI lista_activitati;
	std::vector <unique_ptr<Undo>> undo;
	std::vector <unique_ptr<Undo>> redo;

	std::vector <Activitate> Filter(std::function <bool(const Activitate&)> f) const;

	std::vector <Activitate> Sort(functie f) const;

public:
	Service(Repository& repository, Validator& validator) noexcept : repository{ repository }, validator{ validator }, lista_activitati{ repository } {};
	Service(const Service& service) = delete;

	void adaugare_activitate(const Activitate& activitate);

	const std::vector <Activitate>& get_all() const noexcept;

	int actualizare_activitate(const std::string titlu, const Activitate& Activitate);

	int stergere_activitate(const std::string titlu);

	int get_by_title(const std::string titlu);

	std::vector <Activitate> filtrare_dupa_durata(int durata) const;

	std::vector <Activitate>  filtrare_dupa_tip(const std::string tip);

	std::vector <Activitate> sortare_dupa_titlu() const;

	std::vector <Activitate> sortare_dupa_descriere() const;

	std::vector <Activitate> sortare_dupa_tip_durata() const;

	std::unordered_map <std::string, int> raport() const;

	const std::vector<Activitate>& adaugare_activitate_lista(const std::string& titlu);

	const std::vector<Activitate>& generare_random(size_t numar);

	const std::vector<Activitate>& get_all_lista_activitati() noexcept;

	const std::vector<Activitate>& stergere_lista_activitati() noexcept;

	void ExportCSV();

	void Undo();

	void Redo();
};

