#ifndef MODELLISTA_H
#define MODELLISTA_H
#include <QAbstractListModel>
#include <vector>
#include <qdebug.h>
#include "Domain.h"
#include "Service.h"

class ModelLista : public QAbstractListModel {
    std::vector<Activitate> lista_de_activitati;

public:

    ModelLista(const std::vector<Activitate>& activitati_) : 
        lista_de_activitati{ activitati_ } 
    {};

    int rowCount(const QModelIndex& parent = QModelIndex()) const override {
        return lista_de_activitati.size();
    }

    QVariant data(const QModelIndex& index, int role) const override {
        if (role == Qt::DisplayRole) {
            auto activitate = lista_de_activitati[index.row()].get_titlu();
            return QString::fromStdString(activitate);
        }

        if (role == Qt::UserRole) {
            auto tip = lista_de_activitati[index.row()].get_tip();
            return QString::fromStdString(tip);
        }
        return QVariant{};
    }

    void setActivitati(const vector<Activitate>& activitati) {
        this->lista_de_activitati = activitati;
        QModelIndex topLeft = createIndex(0, 0);
        QModelIndex bottomRight = createIndex(rowCount(), 1);
        emit dataChanged(topLeft, bottomRight);
    }
};

#endif // MODELLISTA_H