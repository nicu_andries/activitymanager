#include "LaboratorGUI.h"
#include <QtWidgets/QApplication>
#include <QtWidgets/qlabel.h>
#include <QtWidgets/qlineedit.h>
#include <QtWidgets/qlistwidget.h>
#include <QtWidgets/qtablewidget.h>
#include <QtWidgets/qpushbutton.h>
#include <QtWidgets/qcombobox.h>
#include <QtWidgets/qboxlayout.h>
#include <QtWidgets\qformlayout.h>
#include <QtWidgets\qgridlayout.h>
#include <QtWidgets/qmessagebox.h>
#include <QtWidgets/qabstractitemdelegate.h>
#include <QTextCursor>
#include "Domain.h"
#include "Observer.h"
#include "CosCRUDGUI.h"
#include "Exceptii.h"
#include <string>


using namespace std;


LaboratorGUI::LaboratorGUI(Service& service, QWidget* parent) : service{ service }, QMainWindow(parent)
{
    ui.setupUi(this);
}


void LaboratorGUI::adaugare_lista() {
	this->service.adaugare_activitate_lista(lista->currentItem()->text().toStdString().substr(0, lista->currentItem()->text().toStdString().find(" ")));
	tabel_activitati->clear();
	tabel_activitati->setItem(0, 0, new QTableWidgetItem("Titlu"));
	tabel_activitati->setItem(0, 1, new QTableWidgetItem("Descriere"));
	tabel_activitati->setItem(0, 2, new QTableWidgetItem("Tip"));
	tabel_activitati->setItem(0, 3, new QTableWidgetItem("Durata"));
	tabel_activitati->item(0, 0)->setBackground(Qt::red);
	tabel_activitati->item(0, 1)->setBackground(Qt::red);
	tabel_activitati->item(0, 2)->setBackground(Qt::red);
	tabel_activitati->item(0, 3)->setBackground(Qt::red);
	const vector <Activitate>& lista_activitati = this->service.get_all_lista_activitati();
	int i = 1;
	for (Activitate activitate : lista_activitati) {
		int j = 0;
		QTableWidgetItem* titlu = new QTableWidgetItem;
		QTableWidgetItem* descriere = new QTableWidgetItem;
		QTableWidgetItem* tip = new QTableWidgetItem;
		QTableWidgetItem* durata = new QTableWidgetItem;
		titlu->setText(QString::fromStdString(activitate.get_titlu()));
		descriere->setText(QString::fromStdString(activitate.get_descriere()));
		tip->setText(QString::fromStdString(activitate.get_tip()));
		durata->setText(QString::number(activitate.get_durata()));
		tabel_activitati->setItem(i, j, titlu);
		tabel_activitati->setItem(i, j + 1, descriere);
		tabel_activitati->setItem(i, j + 2, tip);
		tabel_activitati->setItem(i, j + 3, durata);
		i++;
	}
}


void LaboratorGUI::stergere_lista() {
	this->service.stergere_lista_activitati();
	tabel_activitati->clear();
}


void LaboratorGUI::generare_lista() {
	int numar = this->box_numar->text().toInt();
	this->service.generare_random(numar);
	this->box_numar->clear();
	tabel_activitati->clear();
	tabel_activitati->setItem(0, 0, new QTableWidgetItem("Titlu"));
	tabel_activitati->setItem(0, 1, new QTableWidgetItem("Descriere"));
	tabel_activitati->setItem(0, 2, new QTableWidgetItem("Tip"));
	tabel_activitati->setItem(0, 3, new QTableWidgetItem("Durata"));
	tabel_activitati->item(0, 0)->setBackground(Qt::red);
	tabel_activitati->item(0, 1)->setBackground(Qt::red);
	tabel_activitati->item(0, 2)->setBackground(Qt::red);
	tabel_activitati->item(0, 3)->setBackground(Qt::red);
	const vector <Activitate>& lista_activitati = this->service.get_all_lista_activitati();
	int i = 1;
	for (Activitate activitate : lista_activitati) {
		int j = 0;
		QTableWidgetItem* titlu = new QTableWidgetItem;
		QTableWidgetItem* descriere = new QTableWidgetItem;
		QTableWidgetItem* tip = new QTableWidgetItem;
		QTableWidgetItem* durata = new QTableWidgetItem;
		titlu->setText(QString::fromStdString(activitate.get_titlu()));
		descriere->setText(QString::fromStdString(activitate.get_descriere()));
		tip->setText(QString::fromStdString(activitate.get_tip()));
		durata->setText(QString::number(activitate.get_durata()));
		tabel_activitati->setItem(i, j, titlu);
		tabel_activitati->setItem(i, j + 1, descriere);
		tabel_activitati->setItem(i, j + 2, tip);
		tabel_activitati->setItem(i, j + 3, durata);
		i++;
	}
}


void LaboratorGUI::export_fisier() {
	this->service.ExportCSV();
}


void LaboratorGUI::actualizare_activitate() {
	//QString& titlu_vechi = lista->currentItem()->text();
	//this->service.stergere_activitate(titlu_vechi.toStdString().substr(0, titlu_vechi.toStdString().find(" ")));
	this->service.stergere_activitate(lista->currentItem()->text().toStdString().substr(0, lista->currentItem()->text().toStdString().find(" ")));
	string titlu = this->box_title->text().toStdString();
	string descriere = this->box_descriere->text().toStdString();
	string tip = this->box_tip->text().toStdString();
	int durata = this->box_durata->text().toInt();
	this->service.adaugare_activitate(Activitate(titlu, descriere, tip, durata));
	this->box_title->clear();
	this->box_descriere->clear();
	this->box_tip->clear();
	this->box_durata->clear();
	populate();
}

void LaboratorGUI::unselect(QListWidgetItem*) {
	if (lista->currentItem()->isSelected() == true) {
		this->box_title->clear();
		this->box_descriere->clear();
		this->box_tip->clear();
		this->box_durata->clear();
	}
}


void LaboratorGUI::double_click(QListWidgetItem*) {
	//QString& titlu_vechi = lista->currentItem()->text();
	//int indice_activitate = this->service.get_by_title(titlu_vechi.toStdString().substr(0, titlu_vechi.toStdString().find(" ")));
	int indice_activitate = this->service.get_by_title(lista->currentItem()->text().toStdString().substr(0, lista->currentItem()->text().toStdString().find(" ")));
	Activitate activitate = this->service.get_all().at(indice_activitate);
	this->box_title->setText(QString::fromStdString(activitate.get_titlu()));
	this->box_descriere->setText(QString::fromStdString(activitate.get_descriere()));
	this->box_tip->setText(QString::fromStdString(activitate.get_tip()));
	this->box_durata->setText(QString::number(activitate.get_durata()));
}


void LaboratorGUI::stergere_activitate() {
//	QString& titlu = lista->currentItem()->text();
//	this->service.stergere_activitate(titlu.toStdString().substr(0, titlu.toStdString().find(" ")));
	this->service.stergere_activitate(lista->currentItem()->text().toStdString().substr(0, lista->currentItem()->text().toStdString().find(" ")));
	populate();
	this->box_title->clear();
	this->box_descriere->clear();
	this->box_tip->clear();
	this->box_durata->clear();
}


void LaboratorGUI::undo() {
	this->service.Undo();
	this->populate();
}

void LaboratorGUI::redo() {
	this->service.Redo();
	this->populate();
}

void LaboratorGUI::populate_tabel() {
	tabel->clear();
	tabel->setItem(0, 0, new QTableWidgetItem("Titlu"));
	tabel->setItem(0, 1, new QTableWidgetItem("Descriere"));
	tabel->setItem(0, 2, new QTableWidgetItem("Tip"));
	tabel->setItem(0, 3, new QTableWidgetItem("Durata"));
	const vector <Activitate>& lista_activitati = this->service.get_all();
	int i = 1;
	for (Activitate activitate : lista_activitati) {
		int j = 0;
		QTableWidgetItem* titlu = new QTableWidgetItem;
		QTableWidgetItem* descriere = new QTableWidgetItem;
		QTableWidgetItem* tip = new QTableWidgetItem;
		QTableWidgetItem* durata = new QTableWidgetItem;
		titlu->setText(QString::fromStdString(activitate.get_titlu()));
		descriere->setText(QString::fromStdString(activitate.get_descriere()));
		tip->setText(QString::fromStdString(activitate.get_tip()));
		durata->setText(QString::number(activitate.get_durata()));
		tabel->setItem(i, j, titlu);
		tabel->setItem(i, j+1, descriere);
		tabel->setItem(i, j+2, tip);
		tabel->setItem(i, j+3, durata);
		i++;
	}
}


void LaboratorGUI::populate() {
	this->lista->clear();
	const vector <Activitate>& lista_activitati = this->service.get_all();
	for (Activitate activitate : lista_activitati) {
		lista->addItem(QString::fromStdString(activitate.get_str()));
	}
}


void LaboratorGUI::clear_window() {
	this->lista_sortari->clear();
}

void LaboratorGUI::filtrare_color() {
	this->lista->clear();
	int i = 0;
	string tip = this->box_tip->text().toStdString();
	const vector <Activitate>& lista_activitati = this->service.get_all();
	for (Activitate activitate : lista_activitati) {
		lista->addItem(QString::fromStdString(activitate.get_str()));
		if (activitate.get_tip() == tip)
			lista->item(i)->setBackground(Qt::green);
		i++;
	}
	this->box_tip->clear();
}


void LaboratorGUI::filtrare_durata() {
	tabel->clear();
	tabel->setItem(0, 0, new QTableWidgetItem("Titlu"));
	tabel->setItem(0, 1, new QTableWidgetItem("Descriere"));
	tabel->setItem(0, 2, new QTableWidgetItem("Tip"));
	tabel->setItem(0, 3, new QTableWidgetItem("Durata"));
	int durata = this->box_filtrare_durata->text().toInt();
	const vector <Activitate>& lista_activitati = this->service.filtrare_dupa_durata(durata);
	int i = 1;
	for (Activitate activitate : lista_activitati) {
		int j = 0;
		QTableWidgetItem* titlu = new QTableWidgetItem;
		QTableWidgetItem* descriere = new QTableWidgetItem;
		QTableWidgetItem* tip = new QTableWidgetItem;
		QTableWidgetItem* durata = new QTableWidgetItem;
		titlu->setText(QString::fromStdString(activitate.get_titlu()));
		descriere->setText(QString::fromStdString(activitate.get_descriere()));
		tip->setText(QString::fromStdString(activitate.get_tip()));
		durata->setText(QString::number(activitate.get_durata()));
		tabel->setItem(i, j, titlu);
		tabel->setItem(i, j + 1, descriere);
		tabel->setItem(i, j + 2, tip);
		tabel->setItem(i, j + 3, durata);
		i++;
	}
	box_filtrare_durata->clear();
}


void LaboratorGUI::filtrare_tip() {
	tabel->clear();
	tabel->setItem(0, 0, new QTableWidgetItem("Titlu"));
	tabel->setItem(0, 1, new QTableWidgetItem("Descriere"));
	tabel->setItem(0, 2, new QTableWidgetItem("Tip"));
	tabel->setItem(0, 3, new QTableWidgetItem("Durata"));
	string tip = this->box_filtrare_tip->text().toStdString();
	const vector <Activitate>& lista_activitati = this->service.filtrare_dupa_tip(tip);
	int i = 1;
	for (Activitate activitate : lista_activitati) {
		int j = 0;
		QTableWidgetItem* titlu = new QTableWidgetItem;
		QTableWidgetItem* descriere = new QTableWidgetItem;
		QTableWidgetItem* tip = new QTableWidgetItem;
		QTableWidgetItem* durata = new QTableWidgetItem;
		titlu->setText(QString::fromStdString(activitate.get_titlu()));
		descriere->setText(QString::fromStdString(activitate.get_descriere()));
		tip->setText(QString::fromStdString(activitate.get_tip()));
		durata->setText(QString::number(activitate.get_durata()));
		tabel->setItem(i, j, titlu);
		tabel->setItem(i, j + 1, descriere);
		tabel->setItem(i, j + 2, tip);
		tabel->setItem(i, j + 3, durata);
		i++;
	}
	box_filtrare_tip->clear();
}


void LaboratorGUI::sortare_titlu() {
	tabel->clear();
	tabel->setItem(0, 0, new QTableWidgetItem("Titlu"));
	tabel->setItem(0, 1, new QTableWidgetItem("Descriere"));
	tabel->setItem(0, 2, new QTableWidgetItem("Tip"));
	tabel->setItem(0, 3, new QTableWidgetItem("Durata"));
	const vector <Activitate>& lista_activitati = this->service.sortare_dupa_titlu();
	int i = 1;
	for (Activitate activitate : lista_activitati) {
		int j = 0;
		QTableWidgetItem* titlu = new QTableWidgetItem;
		QTableWidgetItem* descriere = new QTableWidgetItem;
		QTableWidgetItem* tip = new QTableWidgetItem;
		QTableWidgetItem* durata = new QTableWidgetItem;
		titlu->setText(QString::fromStdString(activitate.get_titlu()));
		descriere->setText(QString::fromStdString(activitate.get_descriere()));
		tip->setText(QString::fromStdString(activitate.get_tip()));
		durata->setText(QString::number(activitate.get_durata()));
		tabel->setItem(i, j, titlu);
		tabel->setItem(i, j + 1, descriere);
		tabel->setItem(i, j + 2, tip);
		tabel->setItem(i, j + 3, durata);
		i++;
	}
}


void LaboratorGUI::sortare_descriere() {
	tabel->clear();
	tabel->setItem(0, 0, new QTableWidgetItem("Titlu"));
	tabel->setItem(0, 1, new QTableWidgetItem("Descriere"));
	tabel->setItem(0, 2, new QTableWidgetItem("Tip"));
	tabel->setItem(0, 3, new QTableWidgetItem("Durata"));
	const vector <Activitate>& lista_activitati = this->service.sortare_dupa_descriere();
	int i = 1;
	for (Activitate activitate : lista_activitati) {
		int j = 0;
		QTableWidgetItem* titlu = new QTableWidgetItem;
		QTableWidgetItem* descriere = new QTableWidgetItem;
		QTableWidgetItem* tip = new QTableWidgetItem;
		QTableWidgetItem* durata = new QTableWidgetItem;
		titlu->setText(QString::fromStdString(activitate.get_titlu()));
		descriere->setText(QString::fromStdString(activitate.get_descriere()));
		tip->setText(QString::fromStdString(activitate.get_tip()));
		durata->setText(QString::number(activitate.get_durata()));
		tabel->setItem(i, j, titlu);
		tabel->setItem(i, j + 1, descriere);
		tabel->setItem(i, j + 2, tip);
		tabel->setItem(i, j + 3, durata);
		i++;
	}
}


void LaboratorGUI::sortare_tip_durata() {
	tabel->clear();
	tabel->setItem(0, 0, new QTableWidgetItem("Titlu"));
	tabel->setItem(0, 1, new QTableWidgetItem("Descriere"));
	tabel->setItem(0, 2, new QTableWidgetItem("Tip"));
	tabel->setItem(0, 3, new QTableWidgetItem("Durata"));
	const vector <Activitate>& lista_activitati = this->service.sortare_dupa_tip_durata();
	int i = 1;
	for (Activitate activitate : lista_activitati) {
		int j = 0;
		QTableWidgetItem* titlu = new QTableWidgetItem;
		QTableWidgetItem* descriere = new QTableWidgetItem;
		QTableWidgetItem* tip = new QTableWidgetItem;
		QTableWidgetItem* durata = new QTableWidgetItem;
		titlu->setText(QString::fromStdString(activitate.get_titlu()));
		descriere->setText(QString::fromStdString(activitate.get_descriere()));
		tip->setText(QString::fromStdString(activitate.get_tip()));
		durata->setText(QString::number(activitate.get_durata()));
		tabel->setItem(i, j, titlu);
		tabel->setItem(i, j + 1, descriere);
		tabel->setItem(i, j + 2, tip);
		tabel->setItem(i, j + 3, durata);
		i++;
	}
}


void LaboratorGUI::sortari_window() {
	QHBoxLayout* horizontal_layout = new QHBoxLayout;
	window2->setLayout(horizontal_layout);

	home_submeniu = meniu_sortari->addMenu("&Home");
	QAction* dute_window1 = new QAction("Go to main window");
	home_submeniu->addAction(dute_window1);
	horizontal_layout->setMenuBar(meniu_sortari);


	QPushButton* buton_sortare_titlu = new QPushButton("&Sortare dupa titlu");
	QPushButton* buton_sortare_descriere = new QPushButton("&Sortare dupa descriere");
	QPushButton* buton_sortare_tip_durata = new QPushButton("&Sortare dupa tip/durata");
	QPushButton* buton_filtrare_durata = new QPushButton("&Filtrare dupa durata");
	QPushButton* buton_filtrare_tip = new QPushButton("&Filtrare dupa tip");
	QPushButton* reset_button = new QPushButton("&Reset");

	QWidget* details = new QWidget;
	QFormLayout* form_layout = new QFormLayout;
	details->setLayout(form_layout);
	QLabel* label_tip = new QLabel("Filtru tip:");
	form_layout->addRow(label_tip, box_filtrare_tip);
	QLabel* label_durata = new QLabel("Filtru durata:");
	form_layout->addRow(label_durata, box_filtrare_durata);
	form_layout->addWidget(buton_sortare_titlu);
	form_layout->addWidget(buton_sortare_descriere);
	form_layout->addWidget(buton_sortare_tip_durata);
	form_layout->addWidget(buton_filtrare_durata);
	form_layout->addWidget(buton_filtrare_tip);
	tabel->setFixedSize(441, 327);
	form_layout->addWidget(reset_button);
	horizontal_layout->addWidget(details);
	horizontal_layout->addWidget(tabel);

	QObject::connect(reset_button, SIGNAL(clicked()), this, SLOT(populate_tabel()));
	QObject::connect(buton_sortare_titlu, SIGNAL(clicked()), this, SLOT(sortare_titlu()));
	QObject::connect(buton_sortare_descriere, SIGNAL(clicked()), this, SLOT(sortare_descriere()));
	QObject::connect(buton_sortare_tip_durata, SIGNAL(clicked()), this, SLOT(sortare_tip_durata()));
	QObject::connect(buton_filtrare_durata, SIGNAL(clicked()), this, SLOT(filtrare_durata()));
	QObject::connect(buton_filtrare_tip, SIGNAL(clicked()), this, SLOT(filtrare_tip()));
	QObject::connect(dute_window1, SIGNAL(triggered()), this, SLOT(schimba_window()));

	window2->show();
}


void LaboratorGUI::adaugare_activitate() {
	string titlu = this->box_title->text().toStdString();
	string descriere = this->box_descriere->text().toStdString();
	string tip = this->box_tip->text().toStdString();
	int durata = this->box_durata->text().toInt();
	try {
		this->service.adaugare_activitate(Activitate(titlu, descriere, tip, durata));
	}
	catch (const RepositoryException&) {
		QMessageBox::information(this, tr("Error!"), tr("Activitatea deja exista!"));
	}
	this->populate();
	this->box_title->clear();
	this->box_descriere->clear();
	this->box_tip->clear();
	this->box_durata->clear();
}


void LaboratorGUI::show_window(QApplication& aplicatie) {
	if (window2->isVisible()) 
		window2->close();
	populate();
	QHBoxLayout* horizontal_layout = new QHBoxLayout;
	window1->setLayout(horizontal_layout);

	file_submeniu = menu->addMenu("&File");
	QAction* save_file = new QAction("Save to file");
	file_submeniu->addAction(save_file);

	edit_submeniu = menu->addMenu("&Edit");
	QAction* redo_action = new QAction("Redo");
	QAction* undo_action = new QAction("Undo");
	edit_submeniu->addAction(redo_action);
	edit_submeniu->addAction(undo_action);

	view_submeniu = menu->addMenu("&Filters/Reports");
	QAction* sortari = new QAction("Sortari");
	view_submeniu->addAction(sortari);

	horizontal_layout->setMenuBar(menu);

	QWidget* details = new QWidget;
	QFormLayout* form_layout = new QFormLayout;
	details->setLayout(form_layout);
	QLabel* label_title = new QLabel("Titlu:");
	form_layout->addRow(label_title, box_title);
	QLabel* label_descriere = new QLabel("Descriere:");
	form_layout->addRow(label_descriere, box_descriere);
	QLabel* label_tip = new QLabel("Tip:");
	form_layout->addRow(label_tip, box_tip);
	QLabel* label_durata = new QLabel("Durata:");
	form_layout->addRow(label_durata, box_durata);

	QPushButton* buton_adauga = new QPushButton("&Adauga");
	form_layout->addWidget(buton_adauga);
	QPushButton* buton_sterge = new QPushButton("&Sterge");
	form_layout->addWidget(buton_sterge);
	QPushButton* buton_actualizare = new QPushButton("&Actualizare");
	form_layout->addWidget(buton_actualizare);
	QPushButton* buton_filtrare_color = new QPushButton("&Filtrare color");
	form_layout->addWidget(buton_filtrare_color);
	QPushButton* buton_reset = new QPushButton("&Reset");
	form_layout->addWidget(buton_reset);
	QPushButton* buton_exit = new QPushButton("&Exit");
	form_layout->addWidget(buton_exit);
	details->setFixedSize(200, 300);
	lista->setFixedWidth(300);
	horizontal_layout->addWidget(details);
	horizontal_layout->addWidget(lista);

	QWidget* details2 = new QWidget;
	QFormLayout* form_layout2 = new QFormLayout;
	details2->setLayout(form_layout2);
	QLabel* label_numar = new QLabel("Numar:");
	form_layout2->addRow(label_numar, box_numar);
	QPushButton* buton_generare_lista = new QPushButton("&Generare aleatoare");
	buton_generare_lista->setAutoFillBackground(true);
	buton_generare_lista->setStyleSheet("background-color: rgb(251, 241, 43)");
	form_layout2->addWidget(buton_generare_lista);
	QPushButton* buton_adauga_lista = new QPushButton("&Adauga in lista");
	buton_adauga_lista->setAutoFillBackground(true);
	buton_adauga_lista->setStyleSheet("background-color: rgb(251, 241, 43)");
	form_layout2->addWidget(buton_adauga_lista);
	QPushButton* buton_sterge_lista = new QPushButton("&Sterge lista");
	buton_sterge_lista->setAutoFillBackground(true);
	buton_sterge_lista->setStyleSheet("background-color: rgb(251, 241, 43)");
	form_layout2->addWidget(buton_sterge_lista);
	QPushButton* buton_cos_crud = new QPushButton("&Cos CRUD");
	buton_cos_crud->setAutoFillBackground(true);
	buton_cos_crud->setStyleSheet("background-color: rgb(251, 241, 43)");
	form_layout2->addWidget(buton_cos_crud);
	QPushButton* buton_cos_desenat = new QPushButton("&Cos desenat");
	buton_cos_desenat->setAutoFillBackground(true);
	buton_cos_desenat->setStyleSheet("background-color: rgb(251, 241, 43)");
	form_layout2->addWidget(buton_cos_desenat);
	QPushButton* buton_cos_model = new QPushButton("&Cos model");
	buton_cos_model->setAutoFillBackground(true);
	buton_cos_model->setStyleSheet("background-color: rgb(251, 241, 43)");
	form_layout2->addWidget(buton_cos_model);
	horizontal_layout->addWidget(details2);

	QObject::connect(buton_exit, SIGNAL(clicked()), &aplicatie, SLOT(quit()));
	QObject::connect(buton_adauga, SIGNAL(clicked()), this, SLOT(adaugare_activitate()));
	QObject::connect(buton_sterge, SIGNAL(clicked()), this, SLOT(stergere_activitate()));
	QObject::connect(buton_actualizare, SIGNAL(clicked()), this, SLOT(actualizare_activitate()));
	QObject::connect(buton_filtrare_color, SIGNAL(clicked()), this, SLOT(filtrare_color()));
	QObject::connect(buton_reset, SIGNAL(clicked()), this, SLOT(populate()));
	QObject::connect(buton_adauga_lista, &QPushButton::clicked, [=]() {
		adaugare_lista();
		notify();
		});
	QObject::connect(buton_generare_lista, &QPushButton::clicked, [=]() {
		generare_lista();
		notify();
		});
	QObject::connect(buton_sterge_lista, &QPushButton::clicked, [=]() {
		stergere_lista();
		notify();
		});
	QObject::connect(buton_cos_desenat, &QPushButton::clicked, [=]() {
		addObserver(new CosDesenat{ service, observers });
		});
	QObject::connect(buton_cos_crud, &QPushButton::clicked, [=]() {
		addObserver(new RetetaGUI{ service, observers });
		});
	QObject::connect(buton_cos_model, &QPushButton::clicked, [=]() {
		addObserver(new CosModel{ service, observers });
		});
	QObject::connect(lista, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(double_click(QListWidgetItem*)));
	QObject::connect(lista, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(unselect(QListWidgetItem*)));
	QObject::connect(redo_action, SIGNAL(triggered()), this, SLOT(redo()));
	QObject::connect(undo_action, SIGNAL(triggered()), this, SLOT(undo()));
	QObject::connect(save_file, SIGNAL(triggered()), this, SLOT(export_fisier()));
	QObject::connect(sortari, SIGNAL(triggered()), this, SLOT(schimba_window()));

	window1->setWindowTitle("Aplicatie de activitati!");
	window1->show();
}

void LaboratorGUI::schimba_window() {
	if (window2->isVisible()) {
		window1->show();
		window2->close();
	}
	else if (window1->isVisible()) {
		window2->show();
		populate_tabel();
		window1->close();
	}
}

LaboratorGUI::~LaboratorGUI() {}
