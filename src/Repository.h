#pragma once

#include "Domain.h"
#include <algorithm>
#include <vector>

using namespace std;

class Repository {
public:

	virtual void adaugare_activitate(const Activitate& activitate) = 0;

	virtual int stergere_activitate(const std::string titlu) = 0;

	virtual int actualizare_activitate(const std::string titlu, const Activitate& activitate) = 0;

	virtual const vector<Activitate>& get_all() noexcept = 0;

	virtual ~Repository() = default;
};


class RepositoryMemory : public Repository {
private:
	std::vector <Activitate> lista;

public:
	//RepositoryMemory() = default;
	RepositoryMemory(const RepositoryMemory& repository) = delete;

	RepositoryMemory() {
		citire_din_fisier();
	}

	void adaugare_activitate(const Activitate& activitate) override;

	const std::vector <Activitate>& get_all() noexcept override;

	int actualizare_activitate(const std::string titlu, const Activitate& activitate) override;

	int stergere_activitate(const std::string titlu) override;

	void citire_din_fisier();

	void scriere_in_fisier();
};
