#pragma once
#include "Repository.h"
#include "Domain.h"
#include "Observer.h"
#include <chrono>
#include <random>

using namespace std;

class CosCRUDGUI : public Observable {

	vector<Activitate> cos_activitati;
	Repository& repository;
public:

	CosCRUDGUI(Repository& repository_) : repository{ repository_ } {};


	void adaugare_activitate(const Activitate& activitate) {
		cos_activitati.push_back(activitate);
	}


	void stergere_lista_activitati() {
		cos_activitati.clear();
	}


	void generare_cos(int numar) {
		int seed = std::chrono::system_clock::now().time_since_epoch().count();
		vector <Activitate> lista_activitati = repository.get_all();
		std::shuffle(lista_activitati.begin(), lista_activitati.end(), std::default_random_engine(seed)); 
		while (cos_activitati.size() < numar && lista_activitati.size()>0) {
			cos_activitati.push_back(lista_activitati.back());
			lista_activitati.pop_back();
		}
	}


	const vector<Activitate>& get_all_lista_activitati() const {
		return cos_activitati;
	}

};

