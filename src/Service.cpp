#include "Service.h"
#include <unordered_map>
#include <vector>
#include "Domain.h"
#include "Observer.h"


using namespace std;

void Service::adaugare_activitate(const Activitate& activitate) {
	validator.validare_activitate(activitate);
	repository.adaugare_activitate(activitate);
	undo.push_back(std::make_unique<UndoAdd>(repository, activitate));
	redo.clear();
}

const std::vector <Activitate>& Service::get_all() const noexcept {
	return repository.get_all();
}

int Service::actualizare_activitate(const std::string titlu, const Activitate& activitate) {
	for (auto& obiect : repository.get_all())
		if (obiect.get_titlu() == titlu) {
			undo.push_back(std::make_unique<UndoUpdate>(repository, activitate.get_titlu(), obiect));
			redo.clear();
			break;
		}
	return repository.actualizare_activitate(titlu, activitate);
}

int Service::stergere_activitate(const std::string titlu) {
	for (auto& element : repository.get_all())
		if (element.get_titlu() == titlu) {
			undo.push_back(std::make_unique<UndoDelete>(repository, element));
			break;
		}
	redo.clear();
	return repository.stergere_activitate(titlu);
}

int Service::get_by_title(const std::string titlu) {
	int i = 0;
	for (auto& el : repository.get_all()) {
		if (el.get_titlu() == titlu)
			return i;
		i++;
	}
	return -1;
}

std::vector <Activitate> Service::Filter(std::function <bool(const Activitate&)> f) const {
	std::vector <Activitate> result;
	std::copy_if(repository.get_all().begin(), repository.get_all().end(), std::back_inserter(result), f);
	return result;
}

std::vector <Activitate> Service::filtrare_dupa_durata(int durata) const {
	return Filter([durata](const Activitate& activitate) {
		return activitate.get_durata() <= durata;
		});
}

std::vector <Activitate> Service::filtrare_dupa_tip(const std::string tip) {
	return Filter([tip](const Activitate& activitate) {
		return activitate.get_tip().find(tip) != std::string::npos || tip.find(activitate.get_tip()) != std::string::npos;
		});
}

std::vector <Activitate> Service::Sort(functie f) const {
	std::vector <Activitate> result{ repository.get_all() };
	if (*f != nullptr)
		std::sort(result.begin(), result.end(), f);
	return result;
}

std::vector <Activitate> Service::sortare_dupa_titlu() const {
	return Sort([](const Activitate& activitate1, const Activitate& activitate2) {
		return activitate1.get_titlu() < activitate2.get_titlu();
		});
}

std::vector <Activitate> Service::sortare_dupa_descriere() const {
	return Sort([](const Activitate& activitate1, const Activitate& activitate2) {
		return activitate1.get_descriere() < activitate2.get_descriere();
		});
}

std::vector<Activitate> Service::sortare_dupa_tip_durata() const {
	return Sort([](const Activitate& activitate1, const Activitate& activitate2) {
		if (activitate1.get_tip() == activitate2.get_tip())
			return activitate1.get_durata() < activitate2.get_durata();
		return activitate1.get_tip() < activitate2.get_tip();
		});
}

const std::vector<Activitate>& Service::adaugare_activitate_lista(const std::string& titlu) {
	const int i = get_by_title(titlu);
	lista_activitati.adaugare_activitate(repository.get_all().at(i));
	return lista_activitati.get_all_lista_activitati();
}


const std::vector<Activitate>& Service::generare_random(size_t numar) {
	lista_activitati.generare_cos(numar);
	return lista_activitati.get_all_lista_activitati();
}


const std::vector<Activitate>& Service::get_all_lista_activitati() noexcept {
	return lista_activitati.get_all_lista_activitati();
}

const std::vector<Activitate>& Service::stergere_lista_activitati() noexcept {
	lista_activitati.stergere_lista_activitati();
	return lista_activitati.get_all_lista_activitati();
}


std::unordered_map<std::string, int> Service::raport() const {
	std::unordered_map<string, int> dictionar;
	for (auto& i : repository.get_all())
		dictionar[i.get_tip()]++;
	return dictionar;
}


void Service::ExportCSV() {
	std::ofstream out("fisier.csv");
	for (auto& element : get_all_lista_activitati()) {
		out << element.get_titlu() << "," << element.get_tip() << "," << element.get_descriere() << "," << element.get_durata() << '\n';
	}
	out.close();
}


void Service::Undo() {
	if (undo.empty()) {
		std::vector <std::string> messages;
		messages.push_back("Nu se mai poate face Undo!");
		throw ValidateException(messages);}
	undo.back()->DoUndo();
	redo.push_back(std::move(undo.back()));
	undo.pop_back();
}


void Service::Redo() {
	if (redo.size() == 0) return;
	redo.back()->DoRedo();
	undo.push_back(std::move(redo.back()));
	redo.pop_back();
}
