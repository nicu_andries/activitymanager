#include "RepositoryDictionar.h"
#include "Exceptii.h"
#include <random>
#include <ctime>


void RepositoryDictionar::exceptie() {
	const double operatie = ((double)rand() / RAND_MAX);
		if (operatie > probabilitate)
			throw ProbabilitateException("Probabilitatea de a arunca o eroare este prea mare!\n");
}


void RepositoryDictionar::adaugare_activitate(const Activitate& activitate) {
	exceptie();
	dictionar.insert(std::pair<std::string, Activitate>(activitate.get_titlu(), activitate));
}


std::vector <Activitate>& RepositoryDictionar::get_all() noexcept {
	vector <Activitate> auxiliar;
	lista = auxiliar;
	for (auto element : dictionar)
		lista.push_back(element.second);
	return lista;
}


int RepositoryDictionar::actualizare_activitate(const std::string titlu, const Activitate& activitate) {
	exceptie();
	int pozitie = static_cast<int>(distance(dictionar.begin(), dictionar.find(titlu)));
	dictionar.erase(titlu);
	dictionar.insert(std::pair<std::string, Activitate>(activitate.get_titlu(), activitate));
	return pozitie;
}


int RepositoryDictionar::stergere_activitate(const std::string titlu) {
	exceptie();
	int pozitie = static_cast<int>(distance(dictionar.begin(), dictionar.find(titlu)));
	dictionar.erase(titlu);
	return pozitie;
}
