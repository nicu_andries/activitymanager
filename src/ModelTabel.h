#pragma once
#include <QAbstractTableModel>
#include "Domain.h"

class MyTableModel : public QAbstractTableModel {
	std::vector<Activitate> activitati;


public:
	MyTableModel() {}
	MyTableModel(const std::vector<Activitate>& activitati_) : activitati{ activitati_ } {};

	int rowCount(const QModelIndex& parent = QModelIndex()) const override {
		return activitati.size();
	}
	int columnCount(const QModelIndex& parent = QModelIndex()) const override {
		return 4;
	}
	QVariant data(const QModelIndex& index = QModelIndex(), int role = Qt::DisplayRole) const override {
		if (role == Qt::DisplayRole) {
			Activitate activitate = activitati[index.row()];
			if (index.column() == 0)
				return QString::fromStdString(activitate.get_titlu());
			if (index.column() == 1)
				return QString::fromStdString(activitate.get_descriere());
			if (index.column() == 2)
				return QString::fromStdString(activitate.get_tip());
			if (index.column() == 3) {
				char numar[10];
				return QString::fromStdString(itoa(activitate.get_durata(), numar, 10));
			}
		}
		return QVariant{};
	}

	QVariant headerData(int section, Qt::Orientation orientation, int role) const {
		if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
		{
			switch (section)
			{
			case 0:
				return "Titlu";
			case 1:
				return "Descriere";
			case 2:
				return "Tip";
			case 3:
				return "Durata";
			default:
				break;
			}
		}
		return QVariant();
	}

	void setActivitati(const std::vector<Activitate>& activitati) {
		this->activitati = activitati;
		QModelIndex topLeft = createIndex(0, 0);
		QModelIndex bottomRight = createIndex(rowCount(), columnCount());
		emit dataChanged(topLeft, bottomRight);
		emit layoutChanged();
	}
	
};