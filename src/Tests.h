#pragma once

#include "Domain.h"

using namespace std;

class Teste {

public:
	Teste() = default;
	Teste(const Teste& Test) = delete;

	void test_domain() const;

	void test_repository_dictionar() const;

	void test_repository() const;

	void test_service() const;

	void test_validator() const;

	void run_all_tests() const;

};


