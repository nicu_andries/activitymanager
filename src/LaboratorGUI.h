#pragma once

#include <QtWidgets/QMainWindow>
#include <QHBoxLayout>
#include <QListWidget>
#include <QTableWidget>
#include <QPushButton>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QDialog>
#include <QColor>
#include <QPainter>
#include "Service.h"
#include "ModelTabel.h"
#include "ui_LaboratorGUI.h"


class RetetaGUI : public Observer {
    QWidget* cos_window = new QWidget();
    QTableWidget* tabel_activitati = new QTableWidget{ 20, 4 };
    QVBoxLayout* cos_layout = new QVBoxLayout();
    Service& service;
    std::vector<Observer*>& observers;
public:
    RetetaGUI(Service& service_, std::vector<Observer*>& observers) : service{ service_ }, observers{ observers } {
        cos_window->setLayout(cos_layout);
        cos_layout->addWidget(tabel_activitati);
        update();
        cos_window->show();
    };

    void update() override {
        tabel_activitati->clear();
        tabel_activitati->setItem(0, 0, new QTableWidgetItem("Titlu"));
        tabel_activitati->setItem(0, 1, new QTableWidgetItem("Descriere"));
        tabel_activitati->setItem(0, 2, new QTableWidgetItem("Tip"));
        tabel_activitati->setItem(0, 3, new QTableWidgetItem("Durata"));
        const vector <Activitate>& lista_activitati = this->service.get_all_lista_activitati();
        int i = 1;
        for (Activitate activitate : lista_activitati) {
            int j = 0;
            QTableWidgetItem* titlu = new QTableWidgetItem;
            QTableWidgetItem* descriere = new QTableWidgetItem;
            QTableWidgetItem* tip = new QTableWidgetItem;
            QTableWidgetItem* durata = new QTableWidgetItem;
            titlu->setText(QString::fromStdString(activitate.get_titlu()));
            descriere->setText(QString::fromStdString(activitate.get_descriere()));
            tip->setText(QString::fromStdString(activitate.get_tip()));
            durata->setText(QString::number(activitate.get_durata()));
            tabel_activitati->setItem(i, j, titlu);
            tabel_activitati->setItem(i, j + 1, descriere);
            tabel_activitati->setItem(i, j + 2, tip);
            tabel_activitati->setItem(i, j + 3, durata);
            i++;
        }
    }
};


class CosModel : public Observer {
    QWidget* cos_window = new QWidget();
    QVBoxLayout* cos_layout = new QVBoxLayout();
    MyTableModel* cos_tabel_model = new MyTableModel();
    QTableView* cos_lista = new QTableView();
    Service& service;
    std::vector<Observer*>& observers;
public:
    CosModel(Service& service_, std::vector<Observer*>& observers) : service{ service_ }, observers{ observers } {
        cos_window->setLayout(cos_layout);
        cos_window->setFixedSize(441, 327);
        cos_layout->addWidget(cos_lista);
        cos_lista->setModel(cos_tabel_model);
        update();
        cos_window->show();
    };

    void update() override {
        cos_tabel_model->setActivitati(service.get_all_lista_activitati());
    }
};


class CosDesenat : public Observer, public QWidget {
    QVBoxLayout* cos_layout = new QVBoxLayout();
    Service& service;
    std::vector<Observer*>& observers;
public:
    CosDesenat(Service& service_, std::vector<Observer*>& observers) : service{ service_ }, observers{ observers } {
        this->setLayout(cos_layout);
        this->setFixedHeight(512);
        this->setFixedWidth(512);
        update();
        this->show();
    }
    void formaRandom(QPainter& p, int i, int j)
    {
        std::random_device rdm;
        std::mt19937 eng{ rdm() };
        std::uniform_int_distribution<int> random(0, 2);
        int forma = random(eng);

        if (forma == 0) {
            p.drawRect(j * 150, i * 150, 150, 150);
        }
        if (forma == 1) {
            p.drawLine(j * 150, i * 150, (j + 1) * 150, (i + 1) * 150);
        }
        if (forma == 2) {
            p.drawEllipse(j * 150, i * 150, 150, 150);
        }
    }
    void paintEvent(QPaintEvent* ev) override {
        QPainter p{ this };
        int n = service.get_all_lista_activitati().size();
        if (n != 0) {
            setFixedHeight(((n + (n % 2)) / 2) * 150);
        }
        for (int i = 0, cnt = 0; cnt < n; i++) {
            for (int j = 0; j < 2 && cnt < n; j++) {
                formaRandom(p, i, j);
                cnt++;
            }
        }
    }
    void update() override {
        this->repaint();
    }
};


class LaboratorGUI : public QMainWindow{ 
    
    Q_OBJECT

public:

    LaboratorGUI(Service&, QWidget *parent = Q_NULLPTR);

    ~LaboratorGUI();

public slots:

    void show_window(QApplication& aplicatie);

    void sortari_window();

    void sortare_titlu();

    void sortare_descriere();

    void sortare_tip_durata();

    void filtrare_durata();

    void filtrare_tip();

    void adaugare_activitate();

    void adaugare_lista();

    void export_fisier();

    void stergere_lista();

    void generare_lista();

    void populate_tabel();

    void actualizare_activitate();

    void stergere_activitate();

    void double_click(QListWidgetItem*);

    void unselect(QListWidgetItem*);

    void filtrare_color();

    void schimba_window();

    void clear_window();

    void undo();

    void redo();

    void populate();

private:
    std::vector<Observer*> observers;
    Ui::LaboratorGUIClass ui;
    Service& service;

    QWidget* cos_window = new QWidget();
    QVBoxLayout* cos_layout = new QVBoxLayout();
    MyTableModel* cos_tabel_model = new MyTableModel();

    QMenuBar* menu = new QMenuBar;
    QMenuBar* meniu_sortari = new QMenuBar;
    QWidget* window1 = new QWidget;
    QWidget* window2 = new QWidget;
    QWidget* window_cos_crud = new QWidget;
    QWidget* window_cos_desenat = new QWidget;
    QMenu* edit_submeniu = new QMenu;
    QMenu* file_submeniu = new QMenu;
    QMenu* view_submeniu = new QMenu;
    QMenu* home_submeniu = new QMenu;
    QTableWidgetItem* titlu = new QTableWidgetItem;
    QTableWidgetItem* descriere = new QTableWidgetItem;
    QTableWidgetItem* tip = new QTableWidgetItem;
    QTableWidgetItem* durata = new QTableWidgetItem;
    QListWidget* lista = new QListWidget;
    QListWidget* lista_sortari = new QListWidget;
    QTableWidget* tabel = new QTableWidget{ 20, 4 };
    QTableWidget* tabel_activitati = new QTableWidget{ 20, 4 };
    QLineEdit* box_title = new QLineEdit;
    QLineEdit* box_descriere = new QLineEdit;
    QLineEdit* box_tip = new QLineEdit;
    QLineEdit* box_durata = new QLineEdit;
    QLineEdit* box_filtrare_tip = new QLineEdit;
    QLineEdit* box_filtrare_durata = new QLineEdit;
    QLineEdit* box_numar = new QLineEdit;

    void addObserver(Observer* obs) {
        observers.push_back(obs);
    }

    void notify() {
        for (auto& observer : observers)
            observer->update();
    }
};
