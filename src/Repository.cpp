#include "Repository.h"
#include "Exceptii.h"
#include <fstream>

void RepositoryMemory::adaugare_activitate(const Activitate& activitate) {
	for (Activitate activitate_lista : lista) 
		if (activitate_lista.get_titlu() == activitate.get_titlu())
			throw RepositoryException("Activitatea data deja exista!\n");
	lista.push_back(activitate);
}

const std::vector <Activitate>& RepositoryMemory::get_all() noexcept {
	return lista;
}

int RepositoryMemory::actualizare_activitate(const std::string titlu, const Activitate& activitate) {
	const int i{ stergere_activitate(titlu) };
	if (i != -1)
		std::vector <Activitate> ::iterator it{ lista.insert(lista.begin() + i, activitate) };
	return i;
}

int RepositoryMemory::stergere_activitate(const std::string titlu) {
	int i = 0;
	for (auto& obiect : lista) {
		if (obiect.get_titlu() == titlu) {
			std::vector <Activitate> ::iterator it{ lista.erase(lista.begin() + i) };
			return i;
		}
		i++;
	}
	return -1;
}


void RepositoryMemory::citire_din_fisier() {
    ifstream fisier("fisier.txt");
	string titlu, descriere, tip;
	int durata;
    if (!fisier.is_open()) {
        return;
    }
    while (fisier >> titlu >> descriere >> tip >> durata) {
        Activitate obiect = Activitate(titlu, descriere, tip, durata);
        lista.push_back(obiect);
    }
    fisier.close();
}

void RepositoryMemory::scriere_in_fisier() {
    ofstream fisier("fisier.txt");
    for (const auto& obiect : lista) 
		fisier << obiect.get_str() << endl;
    fisier.close();
}